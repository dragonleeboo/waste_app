import moment from "moment";
import bodyParser from "body-parser";
import express from "express";
import md5 from "md5";
import cors from "cors";
import queryfn from "./app/query";
import querymulti from "./app/querymulti";
import torsor_destroy from "./app/torsor_destroy";
const server = express();
var fs = require('fs');
// var mkdirp = require('mkdirp');
var dir = "D:/envpic";
// mkdirp(`${dir}/destroypic`, function (err) {
//     if (err) console.error(err)
// });


server.use(
    cors(),
    bodyParser.json(),
    bodyParser.urlencoded({
        extended: true
    })
);
WebApp.connectHandlers.use(Meteor.bindEnvironment(server));

Meteor.startup(() => {
    server.post("/destroypic_set", (req, res) => {
        const {
            body
        } = req;
        var base64Data = body.PIC_VERIFY.replace(/^data:image\/jpeg;base64,/, "");
    
        fs.writeFile(`${dir}/destroypic/` + body.name + '(' + body.type + ').jpeg', base64Data, 'base64', function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log("upload profile complete : " + body.name);
                var data = base64Encode(`${dir}/destroypic/` + body.name + '(' + body.type + ').jpeg');
                res.send({
                    data
                });
            }
        });
    
    });
    
    server.post("/check_person", (req, res) => {
        const {
            body
        } = req;
        const query = `SELECT person.person_id, person.prename, person.firstname, person.lastname, person.phone, person.type_job as job_type, person.company_code, company.company_name FROM person LEFT JOIN company ON person.company_code = company.company_id  WHERE person.rfid = '${body.rfid}'`;
        console.log(query);
        queryfn(res, query);
    });
    
    server.post("/check_car", (req, res) => {
        const {
            body
        } = req;
        const query = `SELECT * FROM car JOIN company ON car.company_id = company.company_id WHERE rfid = '${body.rfid}'`;
        console.log(query);
        queryfn(res, query);
    });
    
    server.post("/check_hospital", (req, res) => {
        const {
            body
        } = req;
        const query = `SELECT * FROM hospital WHERE HOSPCODE = '${body.hospcode}'`;
        console.log(query);
        queryfn(res, query);
    });
    
    
    server.post("/insert_transaction", (req, res) => {
        const {
            body
        } = req;
        const query = `INSERT INTO transaction ( transaction.report_id, transaction.url, transaction.driver, transaction.carrier, transaction.car, transaction.hosp_staff, transaction.walkin_date, transaction.walkout_time, transaction.walkout_weight, transaction.status1 ) VALUES ( '${body.report_id}', '${body.url}', '${body.driver}', '${body.carrier}', '${body.car}', '${body.hosp_staff}', '${body.walkin_date}', '${body.walkout_time}', ${body.walkout_weight}, '${body.status1}' );`;
        console.log(query);
        queryfn(res, query);
    });
    
    server.post("/insert_trackcar", (req, res) => {
        const {
            body
        } = req;
        const query = `INSERT INTO trackcar ( trackcar.car_id, trackcar.report_id, trackcar.timestamp) VALUES ( '${body.car_id}', '${body.report_id}', '${body.timestamp}');`;
        console.log(query);
        queryfn(res, query);
    });
    
    server.post("/findcarfortransfer", (req, res) => {
        const {
            body
        } = req;
        const query = `SELECT*FROM (
            SELECT*FROM trackcar WHERE TIMESTAMP IN (
            SELECT MAX(TIMESTAMP) FROM trackcar GROUP BY report_id)) lastcar JOIN car ON car.car_id=lastcar.car_id JOIN company ON car.company_id = company.company_id JOIN transaction ON transaction.report_id=lastcar.report_id JOIN person ON transaction.hosp_staff=person.person_id JOIN hospital ON hospital.HOSPCODE=person.company_code WHERE car.rfid="${body.rfid}" AND transaction.destroy_weight IS NULL`;
        console.log(query);
        queryfn(res, query);
    });
    
    
    
    server.post("/update_trackcar", (req, res) => {
        const {
            body
        } = req;
        const query = `INSERT INTO trackcar (  trackcar.report_id, trackcar.car_id,trackcar.timestamp) VALUES ?`;
        console.log(query);
        querymulti(res, query, body.values);
    });
    
    server.post("/torsor_destroy", (req, res) => {
        const {
            body
        } = req;
        var transac = body.list;
        var persen = body.persen;
        var factory = body.factory;
        var destroy_time = body.destroy_time;
        var status3 = body.status3;
        var queries = '';
        for (let index = 0; index < transac.length; index++) {
            const element = transac[index];
            var report_id = element.report_id
            var destroyer = factory.person_id;
            var destroy_date = destroy_time;
            var destroy_weight = parseFloat(element.walkout_weight) + ((parseFloat(persen) / 100) * parseFloat(element.walkout_weight))
            queries += `UPDATE transaction SET destroyer = "${destroyer}", destroy_date = "${destroy_date}", destroy_weight = "${destroy_weight}" , status3 = "${status3}" WHERE report_id = "${report_id}";`
        }
        console.log(queries);
        torsor_destroy(res, queries);
    });




});

function base64Encode(file) {

    var body = fs.readFileSync(file);
    // console.log(file);
    return body.toString('base64');
}