import mysql from 'mysql'

const mysqlconfig = {
    host: Meteor.settings.public.sqlhost,
    user: Meteor.settings.public.sqluser,
    password: Meteor.settings.public.sqlpassword,
    database: Meteor.settings.public.sqldatabase,
    insecureAuth: true
}


const hosp_query = (res, query) => {
    console.log(query);
    return new Promise(function (resolve, reject) {
        const con = mysql.createConnection(mysqlconfig)
        con.connect(function (err) {
            con.query(query, function (err, result, fields) {

                if (err) {
                    console.log({
                        "errorCode": err.code
                    })
                    res.send({
                        "errorCode": err.code
                    })
                } else {
                    // //console.log(result)
                    var a = {
                        data: {
                            
                        },
                      }

                      result.forEach(element => {
                        a.data[element.HOSPNAME + " [" + element.ADDRESS + "]"] = null
                      });
                      
                    resolve(a)
                    res.send(a)

                }
                con.end();
            })
        })
    })
}

export default hosp_query