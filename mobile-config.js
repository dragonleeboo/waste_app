App.info({
    id: 'com.lapa.mainifest',
    name: 'ติดตามมูลฝอยติดเชื้อ',
    version: '1.4.6'
});

// F:\bitbucket\output_breastcancer --server http://203.157.65.37/

/* cordova-plugin-crosswalk-webview@2.4.0
cordova-android-support-gradle-release@3.0.0 */

App.icons({
    'android_mdpi': 'icon/icon.png',
    'android_hdpi': 'icon/icon.png',
    'android_xhdpi': 'icon/icon.png',
    'android_xxhdpi': 'icon/icon.png',
    'android_xxxhdpi': 'icon/icon.png',
});

App.launchScreens({
  'android_mdpi_portrait': 'icon/splash.png',
  'android_mdpi_landscape': 'icon/splash.png',
  'android_hdpi_portrait': 'icon/splash.png',
  'android_hdpi_landscape': 'icon/splash.png',
  'android_xhdpi_portrait': 'icon/splash.png',
  'android_xhdpi_landscape': 'icon/splash.png',
  'android_xxhdpi_portrait': 'icon/splash.png',
  'android_xxhdpi_landscape': 'icon/splash.png'
});

App.setPreference('StatusBarOverlaysWebView', 'true');
App.setPreference('StatusBarStyle', 'lightcontent');
App.setPreference('StatusBarBackgroundColor', '#000000');
App.setPreference('Orientation', 'portrait');

App.setPreference("SplashMaintainAspectRatio" ,"true");
// App.setPreference('SplashScreen', 'none');
App.setPreference("AutoHideSplashScreen" ,"true");
App.setPreference("SplashScreenDelay" ,"2000" );
App.accessRule('http://*', { type: 'navigation' });
App.accessRule('https://*', { type: 'navigation' });
App.accessRule('*');