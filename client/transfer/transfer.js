import Swal from 'sweetalert2'
const axios = require('axios');
// import Swiper from 'swiper';
// var mySwiper = new Swiper('.swiper-container', { 
    
//     direction: 'vertical',
//         loop: true
//  });


// // var mySwiper = new Swiper('.swiper-container', { /* ... */ });
// Template.transfer.onCreated(function () {
    
//     var mySwiper = new Swiper ('.swiper-container', {
//         // Optional parameters
//         direction: 'vertical',
//         loop: true
//       })
    
// });

Template.transfer.onRendered(function () {

    // $('#first').show();
    // $('#two').hide();
    Session.set('carone', null)
    Session.set('cartwo', null)
    if (Meteor.isCordova) {
        MufuCaCa.enabled(function (data) {
            MufuCaCa.addTagDiscoveredListener(function (data) {
                var rfid = MufuCaCa.bytesToHexString(data.tag.id)
                if (!Session.get('carone')) {

                    axios.post(`${Meteor.settings.public.api_host}/findcarfortransfer`, {
                        rfid
                    }).then((data) => {
                        if (data.data.length != 0) {
                            Session.set('carone', {
                                name: data.data[0].car_id,
                                company: data.data[0].company_name
                            })
                            Session.set('caronelist', data.data)
                        }else{
                            Swal.fire({
                                title: "ไม่พบข้อมูลการเก็บขน",
                                text: "รถคันนี้ไม่มีข้อมูลการเก็บขน กรุณาตรวจสอบอีกครั้ง",
                                timer: 1000,
                                showConfirmButton: false,
                                icon: "warning"
                            });
                        }
                    })
                } else {
                    axios.post(`${Meteor.settings.public.api_host}/check_car`, {
                        rfid
                    }).then((data) => {
                        console.log(data.data);
                        if (data.data.length != 0) {
                            Session.set('cartwo', {
                                name: data.data[0].car_id,
                                company: data.data[0].company_name
                            })
                        }
                    })
                }
            });

        }, function () {
            alert("NFC ถูกปิดใช้งาน กรุณาเปิดใช้งานก่อนทุกครั้ง")
        })
    }
});
Template.transfer.helpers({
    carone() {
        return Session.get('carone')
    },
    cartwo() {
        return Session.get('cartwo')
    },
    checkstate() {
        if (Session.get('carone') && Session.get('cartwo')) {
            return true
        }
    },
    caronelist() {
        return Session.get('caronelist')
    },
    sum_weight() {
        var list = Session.get('caronelist')
        var weight = 0;
       
        for (let index = 0; index < list.length; index++) {
            const element = list[index].walkout_weight;
            weight += element
        }
        return weight

    }


});

Template.transfer.events({
    'click #clearcarone': function (event, template) {
        Session.set('carone', null)
        Session.set('caronelist', null)
    },
    'click #clearcartwo': function (event, template) {
        Session.set('cartwo', null)
    },
    'click #confirm'(){
        var list = Session.get('caronelist')
        var date = moment().tz("Asia/Bangkok").format('YYYY-MM-DD HH:mm:ss');
        console.log(list);
        var arr = []
        for (let index = 0; index < list.length; index++) {
            const element = list[index];
            arr.push([element.report_id,Session.get('cartwo').name,date])
        }
        console.log(arr);
        

        axios.post(`${Meteor.settings.public.api_host}/update_trackcar`,{
            values: arr
        }).then((message)=>{
            console.log(message);
            Swal.fire({
                title: "สำเร็จ!",
                // text: "  ID : "+r+" ",
                timer: 3000,
                showConfirmButton: false,
                icon: "success"
            }).then(function () {
                Router.go('/')
            });
        })
        // console.log(arr);
    }
    // 'mouseup #confirm'() {
    //     var pressTimer = Session.get('pressTimer')
    //     clearTimeout(pressTimer);
    //     console.log('fff');

  

    // },
    // 'mousedown #confirm'() {
    //     Session.set('pressTimer',null)
    //     var pressTimer = Session.get('pressTimer')
    //     pressTimer = window.setTimeout(function () {
    //         Session.set('pressTimer',null)
    //         alert('ggggs')
    //     }, 1000);
    // }


    
    // $("#confirm").mouseup(function () {
       
    // }).mousedown(function () {
    //     // Set timeout
        
    //     return false;
    // });
});