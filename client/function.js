const axios = require('axios');
import Swal from 'sweetalert2'
myfuc = (function() {
    var api = {};

    function getDateToday() {
        var today = moment(new Date()).format("DD/MM/YYYY");
        var split_today = today.split('/')
        var date_today = split_today[0] + "/" + split_today[1] + "/" + (parseInt(split_today[2]) + 543)
        return date_today
    }

    api.check_rfid = function(data) {
        axios.post(`${Meteor.settings.public.api_host}/check_person`, {
            rfid: data,
        })
        .then(function (response) {
            console.log('data', response.data)
            return response.data
        })
        .catch(function (error) {
            //console.log(error);
        });
    }

    api.onload = function(data) {
        $('.' + data).hide();
        // $("body").removeClass("grey lighten-5");
        // $("body").addClass("white");
        $(".showload").append('<div class="loading"> <center> <div class="loading"> <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div> </div> </center> </div>');
        $(".showload").fadeIn();
    }

    api.ondone = function(data) {
        $('.' + data).show();
        // $("body").addClass("white");
        // $("body").removeClass("white");
        $(".showload").fadeOut(function() {
            $(".showload").empty();
        })
    }
    return api;
}());
