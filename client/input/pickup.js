const Swal = require('sweetalert2')
const axios = require('axios');
Template.pickup.onRendered(function () {
    if (navigator.onLine) {
        // alert('online');
    } else {
        Swal.fire({
            title: "กรุณาเชื่อมต่ออินเตอร์เน็ต",
            text: "โปรดลองอีกครั้ง หรือติดต่อเจ้าหน้าที่",
            timer: 1000,
            showConfirmButton: false,
            icon: "error"
        }).then(() => {
            Router.go('/')
        });
    }
    Session.set('bleListDevices', null)
    Session.set('bleSerial', null)
    Session.set('car', null)
    Session.set('cardriver', null)
    Session.set('pickupman', null)
    Session.set('hospman', null)
    Session.set('walkin_date', null)
    Session.set('typestatus', [0, 0, 0, 0])
    $('#scan').show();
    $('#wait').hide();

    localStorage.getItem('car') ? Session.set('car', JSON.parse(localStorage.getItem('car'))) : 0;
    localStorage.getItem('cardriver') ? Session.set('cardriver', JSON.parse(localStorage.getItem('cardriver'))) : 0;
    localStorage.getItem('pickupman') ? Session.set('pickupman', JSON.parse(localStorage.getItem('pickupman'))) : 0;

    // check_tag("798f9d8e");
    // check_tag("c9f6968e");
    // check_tag("5771b252");
    // check_tag("277b9e51");

    if (Meteor.isCordova) {



        bluetoothSerial.enable(
            function () {
                console.log("Bluetooth is enabled");
                bluetoothSerial.disconnect();
                MufuCaCa.enabled(function (data) {
                    MufuCaCa.addTagDiscoveredListener(function (data) {
                        var tag = MufuCaCa.bytesToHexString(data.tag.id)
                        check_tag(tag);
                    });

                }, function () {
                    Swal.fire({
                        title: "NFC ถูกปิดใช้งาน",
                        text: "กรุณาเปิดใช้งานก่อนทุกครั้ง หรือติดต่อเจ้าหน้าที่",
                        timer: 2000,
                        showConfirmButton: false,
                        icon: "error"
                    }).then(() => {
                        Router.go('/')
                    });
                })
            },
            function () {
                Swal.fire({
                    title: "เชื่อมต่อบลูทูธล้มเหลว",
                    text: "โปรดลองอีกครั้ง หรือติดต่อเจ้าหน้าที่",
                    timer: 2000,
                    showConfirmButton: false,
                    icon: "error"
                }).then(() => {
                    Router.go('/')
                });
            }
        );
    }

})
Template.pickup.helpers({
    bleSerial: function () {
        return Session.get('bleSerial')
    },
    bleListDevices: function () {
        return Session.get('bleListDevices')
    },
    car: function () {
        return Session.get('car')
    },
    cardriver: function () {
        return Session.get('cardriver')
    },
    pickupman: function () {
        return Session.get('pickupman')
    },
    hospman: function () {
        return Session.get('hospman')
    },
    next() {
        if (Session.get('car') && Session.get('cardriver') && Session.get('pickupman') && Session.get('hospman')) {
            return true
        }
    }
});

Template.pickup.events({
    'click #scan': function (event, template) {
        $('#scan').hide();
        $('#wait').show();
        if (Meteor.isCordova) {
            bluetoothSerial.discoverUnpaired(function (data) {
                $('#scan').show();
                $('#wait').hide();
                Session.set('bleListDevices', data)
            });
        }
    },
    "click #selectble"() {
        if (Meteor.isCordova) {
            bluetoothSerial.connect(this.address, function (data) {}, function (data) {
                console.log(data)
            });
            bluetoothSerial.subscribe('\n', function (data) {
                var dataconvert = data.split('**=')[1]
                var dataconvert2 = dataconvert.split(':**')[0]
                Session.set('bleSerial', dataconvert2)
            });
        }
    },
    "click #clearcar"() {
        Session.set('car', null)
        localStorage.removeItem('car')
    },
    "click #clearcardriver"() {
        Session.set('cardriver', null)
        localStorage.removeItem('cardriver')
    },
    "click #clearpickupman"() {
        Session.set('pickupman', null)
        localStorage.removeItem('pickupman')
    },
    "click #clearhospman"() {
        Session.set('hospman', null)
    },
    "click #start"() {
        var walkin_date = moment().tz("Asia/Bangkok").format('YYYY-MM-DD HH:mm:ss');
        Session.set('walkin_date', walkin_date)
        Router.go('weigh')
    }
});


async function check_tag(tag) {
    myfuc.onload("pickup")
    let promiseA = axios.post(`${Meteor.settings.public.api_host}/check_person`, {
        rfid: tag
    })
    let resultA = await promiseA
    console.log(resultA.data);
    if (resultA.data.length == 0) {
        let promiseB = axios.post(`${Meteor.settings.public.api_host}/check_car`, {
            rfid: tag
        })
        let resultB = await promiseB
        if (resultB.data.length == 0) {
            Swal.fire({
                title: "ไม่พบข้อมูลบัตร!",
                text: "โปรดลองอีกครั้ง หรือติดต่อเจ้าหน้าที่",
                timer: 1000,
                showConfirmButton: false,
                icon: "error"
            });
        } else {
            Swal.fire({
                title: "สำเร็จ!",
                text: resultB.data[0].car_id,
                timer: 1000,
                showConfirmButton: false,
                icon: "success"
            });
            localStorage.setItem('car', JSON.stringify(resultB.data[0]))
            Session.set('car', JSON.parse(localStorage.getItem('car')))
        }
        myfuc.ondone("pickup")
    } else {
        if (resultA.data[0].job_type == "2" || resultA.data[0].job_type == "3") {
            console.log(resultA.data[0].person_id, resultA.data[0].person_id);
            if (!Session.get('cardriver')) {
                if (Session.get('pickupman')) {
                    if (Session.get('pickupman').person_id == resultA.data[0].person_id) {
                        Swal.fire({
                            title: "บัตรซ้ำกับ พนักงานขน!",
                            text: "โปรดลองอีกครั้ง",
                            timer: 1000,
                            showConfirmButton: false,
                            icon: "warning"
                        });
                    } else {
                        Swal.fire({
                            title: "สำเร็จ!",
                            text: resultA.data[0].prename + resultA.data[0].firstname + " " + resultA.data[0].lastname,
                            timer: 1000,
                            showConfirmButton: false,
                            icon: "success"
                        });
                        localStorage.setItem('cardriver', JSON.stringify(resultA.data[0]))
                        Session.set('cardriver', JSON.parse(localStorage.getItem('cardriver')))
                    }
                } else {
                    Swal.fire({
                        title: "สำเร็จ!",
                        text: resultA.data[0].prename + resultA.data[0].firstname + " " + resultA.data[0].lastname,
                        timer: 1000,
                        showConfirmButton: false,
                        icon: "success"
                    });
                    localStorage.setItem('cardriver', JSON.stringify(resultA.data[0]))
                    Session.set('cardriver', JSON.parse(localStorage.getItem('cardriver')))
                }
            } else {
                if (Session.get('cardriver')) {
                    if (Session.get('cardriver').person_id == resultA.data[0].person_id) {
                        Swal.fire({
                            title: "บัตรซ้ำกับ พนักงานขับ!",
                            text: "โปรดลองอีกครั้ง",
                            timer: 1000,
                            showConfirmButton: false,
                            icon: "warning"
                        });
                    } else {
                        Swal.fire({
                            title: "สำเร็จ!",
                            text: resultA.data[0].prename + resultA.data[0].firstname + " " + resultA.data[0].lastname,
                            timer: 1000,
                            showConfirmButton: false,
                            icon: "success"
                        });
                        localStorage.setItem('pickupman', JSON.stringify(resultA.data[0]))
                        Session.set('pickupman', JSON.parse(localStorage.getItem('pickupman')))
                    }
                } else {
                    Swal.fire({
                        title: "สำเร็จ!",
                        text: resultA.data[0].prename + resultA.data[0].firstname + " " + resultA.data[0].lastname,
                        timer: 1000,
                        showConfirmButton: false,
                        icon: "success"
                    });
                    localStorage.setItem('pickupman', JSON.stringify(resultA.data[0]))
                    Session.set('pickupman', JSON.parse(localStorage.getItem('pickupman')))
                }
            }
        } else if (resultA.data[0].job_type == "1") {
            let promiseC = axios.post(`${Meteor.settings.public.api_host}/check_hospital`, {
                hospcode: resultA.data[0].company_code
            })
            let resultC = await promiseC
            // console.log(resultC.data);

            Swal.fire({
                title: "สำเร็จ!",
                text: resultA.data[0].prename + resultA.data[0].firstname + " " + resultA.data[0].lastname,
                timer: 1000,
                showConfirmButton: false,
                icon: "success"
            });
            // localStorage.setItem('hospman', JSON.stringify(resultA.data[0]))
            var hm = resultA.data[0]
            hm.company_name = resultC.data[0].NAME
            Session.set('hospman', resultA.data[0])
        } else {
            Swal.fire({
                title: "ไม่พบข้อมูลบัตร!",
                text: "โปรดลองอีกครั้ง หรือติดต่อเจ้าหน้าที่",
                timer: 1000,
                showConfirmButton: false,
                icon: "error"
            });
        }
        myfuc.ondone("pickup")
    }
}