import Swal from 'sweetalert2'
const axios = require('axios');
Template.pickupsummary.helpers({
    wasetotal: function () {
        return Session.get('wastetotal')
    },

    car: function () {
        return Session.get('car')
    },
    cardriver: function () {
        return Session.get('cardriver')
    },
    pickupman: function () {
        return Session.get('pickupman')
    },
    hospman: function () {
        return Session.get('hospman')
    },
});


Template.pickupsummary.events({
    'click #cancel': function (event, template) {

        Swal.fire({
                title: "ต้องการยกเลิก ?",
                text: "ข้อมูลการทำรายกาารจะหายทั้งหมด",
                type: "warning",
                icon: "warning",
                confirmButtonText: "ใช่แล้ว",
                cancelButtonText: "ไม่ใช่",
                showCancelButton: true
            })
            .then((result) => {
                console.log(result.value);
                if (result.value) {
                    Swal.fire({
                        title: "สำเร็จ!",
                        text: "ยกเลิกรายการเรียบร้อย",
                        timer: 2000,
                        showConfirmButton: false,
                        icon: "success"
                    }).then(function () {
                        Router.go('/')
                    });
                } else if (result.dismiss === 'cancel') {

                }
            })
    },
    'click #send'() {
        myfuc.onload('pickupsummary')
        let r = makeid(10)
        var walkout_time = moment().tz("Asia/Bangkok").format('YYYY-MM-DD HH:mm:ss');
        var obj = {
            report_id: r,
            url: r,
            driver: Session.get('cardriver').person_id,
            carrier: Session.get('pickupman').person_id,
            car: Session.get('car').car_id,
            hosp_staff: Session.get('hospman').person_id,
            walkin_date: Session.get('walkin_date'),
            walkout_time: walkout_time,
            walkout_weight: parseFloat(Session.get('wastetotal')),
            status1: walkout_time
        }
        console.log(obj);
        axios.post(`${Meteor.settings.public.api_host}/insert_transaction`, obj).then(() => {
            axios.post(`${Meteor.settings.public.api_host}/insert_trackcar`, {
                report_id: r,
                car_id: Session.get('car').car_id,
                timestamp: walkout_time
            }).then(() => {
                // copyToClipboard('http://infectiouswaste.anamai.moph.go.th/track=' + r)
                var text = `ตช.จาก ${Session.get('hospman').company_name} โดยรถ ${Session.get('car').car_id} น้ำหนัก ${parseFloat(Session.get('wastetotal')).toFixed(2)} กก. `
                Swal.fire({
                    // title: 'เรียบร้อย',
                    html: `<b style="font-size:20px;color:black;">ดำเนินการเสร็จสิ้น</b>
                        <p style="font-size:18px;color:black;">คิวอาร์โค้ดสำหรับการติดตาม คือ</p>
                        <canvas id="canvas"></canvas>  <br>
                        <b style="font-size:20px;color:#E74C3C;" class='pulse' onclick="window.plugins.socialsharing.share('${text}', null, null, 'http://infectiouswaste.anamai.moph.go.th/track=${r}')">กดที่นี่เพื่อแชร์ลิงก์ <i class="fas fa-share-alt"></i></b>
                        <p style="font-size:18px">http://infectiouswaste.anamai.moph.go.th/track=<b style="color:blue;">${r}<b><p>`,
                    showCancelButton: true,
                    showConfirmButton: false,
                    cancelButtonColor: '#3085d6',
                    cancelButtonText: 'ปิด'
                }).then((result) => {
                    // if (result.value) {
                    // copyToClipboard('http://infectiouswaste.anamai.moph.go.th/track=' + r)
                    bluetoothSerial.write("C");
                    Session.set('stop',true);
                    Router.go('pickup')
                    myfuc.ondone('pickupsummary')
                    // Router.go('/')
                    // }
                })
                // copyToClipboard('http://infectiouswaste.anamai.moph.go.th/track=' + r)
                var QRCode = require('qrcode')
                var canvas = document.getElementById('canvas')
                QRCode.toCanvas(canvas, `http://infectiouswaste.anamai.moph.go.th/track=` + r, function (error) {
                    if (error) console.error(error)
                    console.log('success!');
                    // copyToClipboard('http://infectiouswaste.anamai.moph.go.th/track=' + r)
                    canvas.style.height = '250px'
                    canvas.style.width = '250px'
                })

                // Swal.fire({
                //     title: "สำเร็จ!",
                //     text: "  ID : " + r + " ",
                //     timer: 2000,
                //     showConfirmButton: false,
                //     icon: "success"
                // }).then(function () {
                //     myfuc.ondone('pickupsummary')
                //     Router.go('/')
                // });
            })
        })

        // Swal.fire({
        //     title: '<h4>เรียบร้อย</h4>',
        //     // icon: 'info',
        //     html: '<canvas id="canvas"></canvas> <br> ',
        //     // showCloseButton: true,
        //     // showCancelButton: true,
        //     focusConfirm: false,
        //     confirmButtonText: '<i class="fa fa-thumbs-up"></i> Great!',
        //     confirmButtonAriaLabel: 'Thumbs up, great!'
        // })
        // <canvas id="canvas"></canvas>



    },
    'click #ff'() {
        console.log('cc');

        var copyText = document.getElementById("copy");
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");
        M.toast({
            html: "คัดลอกลิงก์แล้ว"
        })

    }
});


function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

// copyToClipboard('http://infectiouswaste.anamai.moph.go.th/track=')
function copyToClipboard(text) {
    // document.activeElement.blur();
    setTimeout(() => {
        var dummy = document.createElement("textarea");
        // to avoid breaking orgain page when copying more words
        // cant copy when adding below this code
        // dummy.style.display = 'none'
        document.body.appendChild(dummy);
        //Be careful if you use texarea. setAttribute('value', value), which works with "input" does not work with "textarea". – Eduard
        dummy.value = text;
        dummy.select();
        document.execCommand("copy");
        document.body.removeChild(dummy);
        M.toast({
            html: "คัดลอกลิงก์แล้ว"
        })
    }, 1000);


}