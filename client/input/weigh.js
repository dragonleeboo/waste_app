import {
    toast
} from "materialize-css";
import Swal from 'sweetalert2'
Template.weigh.onCreated(function () {
    $(document).ready(function () {
        Session.set('bleSerial', null);
        Session.set('waste', []);
        Session.set('wastetotal', null);
        Session.set('findwait', false);
        Session.set('find', null);
        Session.set('listdevice', JSON.parse(localStorage.getItem('scaledevices')))

        Session.set('dis1', "disabled")
        Session.set('dis2', "")
        Session.set('dis3', "")
        Session.set('dis4', "")
        Session.set('numdif', 0)



    });

    if (Meteor.isCordova) {

        // bluetoothSerial.discoverUnpaired(function (data) {
        //     console.log(data);
        // console.log(data);
        // var check = 0;
        // if (data) {
        //     for (let i = 0; i < data.length; i++) {
        //         const element = data[i];

        //         if (element.name) {
        //             if (element.name == "MOCKUP_SCALE") {
        //                 // M.toast({html: "กำลังค้นหา"})
        //                 localStorage.setItem('bt_address', element.address)
        //                 M.toast({
        //                     html: "ค้นพบเครื่องชั่งแล้ว กำลังเชื่อมต่อ ... " + element.address
        //                 })
        //                 bluetoothSerial.connect(element.address, function (data) {
        //                     M.toast({
        //                         html: "เชื่อมต่อสำเร็จ!!"
        //                     })
        //                     bluetoothSerial.subscribe('\n', function (data) {
        //                         var dataconvert = data.split('**=')[1]
        //                         var dataconvert2 = dataconvert.split(':**')[0]
        //                         Session.set('bleSerial', dataconvert2)
        //                         // console.log(dataconvert2);
        //                         $("#wait").hide();
        //                     });
        //                     check = 1;
        //                 }, function (data) {
        //                     if (data) {
        //                         M.toast({
        //                             html: "กำลังค้นหา ..."
        //                         })
        //                     }
        //                     // M.toast({
        //                     //     html: data
        //                     // })
        //                 });
        //             }
        //         }
        //     }
        // }
        // setTimeout(() => {
        //     console.log(check);
        //     if (check == 0) {
        //         Swal.fire({
        //             title: "โปรดตรวจสอบ!",
        //             text: "ไม่พบเครื่องชั่ง",
        //             timer: 1000,
        //             showConfirmButton: false,
        //             icon: "warning"
        //         }).then(function () {
        //             Router.go('/')
        //         });
        //     }
        // }, 15000);
        // });
    }
});
Template.weigh.helpers({
    bleSerial: function () {
        if (Session.get('bleSerial')) {
            var value = parseFloat(Session.get('bleSerial') - Session.get('numdif')).toFixed(2);
            if (value > 0) {
                return parseFloat(Session.get('bleSerial') - Session.get('numdif')).toFixed(2);
            } else {
                return "0"
            }

        }

    },
    waste: function () {
        return Session.get('waste')
    },
    wasetotal: function () {

        return Session.get('wastetotal')
    },
    find() {
        return Session.get('find')
    },
    findwait() {
        return Session.get('findwait')
    },
    listdevice() {
        return Session.get('listdevice')
    },
    dis1() {
        if (Session.get('dis1')) {
            return "light-blue lighten-4"
        } else {
            return ""
        }
    },
    dis2() {
        if (Session.get('dis2')) {
            return "light-blue lighten-4"
        } else {
            return ""
        }
    },
    dis3() {
        if (Session.get('dis3')) {
            return "light-blue lighten-4"
        } else {
            return ""
        }
    },
    dis4() {
        if (Session.get('dis4')) {
            return "light-blue lighten-4"
        } else {
            return ""
        }
    }
});

Template.weigh.events({
    'click #add': function (event, template) {
        var waste = Session.get('waste');
        var weigh = parseFloat(Session.get('bleSerial') - Session.get('numdif'));
        // var value = parseFloat(Session.get('bleSerial') - Session.get('numdif'));
        if (weigh > 0) {
            weigh = weigh
        } else {
            weigh = 0
        }

        function makeid(length) {
            var result = '';
            var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for (var i = 0; i < length; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        }

        //  console.log(makeid(5));

        waste.push({
            no: makeid(5),
            wasteweigh: weigh
        })
        Session.set('waste', waste);

        var total = 0;
        for (let index = 0; index < waste.length; index++) {
            const element = waste[index];
            total += element.wasteweigh
        }
        Session.set('wastetotal', total);
    },
    "click #clear"() {
        // console.log(Session.get('waste'));
        var waste = Session.get('waste')

        for (var i = waste.length - 1; i >= 0; --i) {
            if (waste[i].no == this.no) {
                waste.splice(i, 1);
            }
        }

        var total = 0;
        for (let index = 0; index < waste.length; index++) {
            const element = waste[index];
            total += element.wasteweigh
        }
        Session.set('wastetotal', total);

        Session.set('waste', waste);

    },
    'click #sumary'() {
        Router.go('pickupsummary')
        // myfuc.onload("weigh")
    },
    'click #find'() {
        M.toast({
            html: "กำลังค้นหา ... "
        })
        Session.set('find', null);
        Session.set('findwait', true);
        bluetoothSerial.discoverUnpaired(function (data) {
            console.log(data);
            var devices = []
            for (let index = 0; index < data.length; index++) {
                const element = data[index];
                if (element.class == "272") {
                    devices.push(element)
                }
            }
            Session.set('find', devices);
            Session.set('findwait', false);
        });
    },
    "click #selectfind"() {
        var scaledevices = [];
        if (localStorage.getItem('scaledevices')) {
            scaledevices = JSON.parse(localStorage.getItem('scaledevices'));
        }
        var findevices = this

        var check = true;
        if (scaledevices.length > 0) {
            console.log('findevices', findevices);
            console.log('scaledevices', scaledevices);
            for (let old = 0; old < scaledevices.length; old++) {
                const olddevices = scaledevices[old];
                // for (let news = 0; news < findevices.length; news++) {
                //     const newdevices = findevices[news];
                // console.log(findevices.address, olddevices.address);
                if (this.address == olddevices.address) {
                    check = false
                }
                // }

            }
        }
        // console.log(check);

        if (check) {
            scaledevices.push(this)
            console.log(scaledevices);
            localStorage.setItem('scaledevices', JSON.stringify(scaledevices))
            Session.set('listdevice', JSON.parse(localStorage.getItem('scaledevices')))
            M.toast({
                html: "เชื่อมต่อสำเร็จ "
            })
        }
        Session.set('find', null)

        // localStorage.setItem('scaledevices', JSON.stringify(resultB.data[0]))
        // Session.set('car', JSON.parse(localStorage.getItem('car')))
    },
    "click #selectdevice"() {
       
        var check = 0;
        myfuc.onload("weigh")
        bluetoothSerial.connect(this.address, function (data) {
            M.toast({
                html: "เชื่อมต่อแล้ว"
            })
            bluetoothSerial.subscribe('\n', function (data) {
                console.log(data);
                if(data[0] == "A"){
                    if(Session.get('stop')){
                        bluetoothSerial.disconnect();
                        Router.go('pickup')
                        myfuc.ondone("weigh")
                    }
                }
                var dataconvert = data.split('**=')[1]
                var dataconvert2 = dataconvert.split(':**')[0]
                if (dataconvert2) {
                    myfuc.ondone("weigh")
                    Session.set('bleSerial', dataconvert2)
                    // console.log(dataconvert2);
                    $(".listdevices").hide();
                    // setTimeout(function () {
                        var man_wei = localStorage.getItem('man_wei');
                        console.log(man_wei == null);
                        if (man_wei == null) {
                            // console.log(man_wei, "555")
                            $('#man_wei').val("0")
                        } else {
                            console.log(man_wei);
            
                            $('#man_wei').val(man_wei)
                            // Session.set('numdif', $('#man_wei').val())
                        }
                        // }, 1000);
                } else {
                    $(".listdevices").hide();
                    bluetoothSerial.disconnect();
                    Session.set('bleSerial', null);
                    Session.set('waste', []);
                    Session.set('wastetotal', null);
                    Session.set('findwait', false);
                    Session.set('find', null);
                    Session.set('listdevice', JSON.parse(localStorage.getItem('scaledevices')))
                }



            });

            
            check = 1;
        }, function (data) {
            if (data) {
                // M.toast({
                //     html: "กำลังค้นหา ..."
                // })
            }
        });

        setTimeout(() => {
            console.log(check);
            if (check == 0) {
                Swal.fire({
                    title: "โปรดตรวจสอบ!",
                    text: "เชื่อมต่อไม่สำเร็จ",
                    timer: 1000,
                    showConfirmButton: false,
                    icon: "warning"
                }).then(function () {
                    // Router.go('/')
                    myfuc.ondone("weigh")
                    $(".listdevices").show();
                    bluetoothSerial.disconnect();
                    Session.set('bleSerial', null);
                    Session.set('waste', []);
                    Session.set('wastetotal', null);
                    Session.set('findwait', false);
                    Session.set('find', null);
                    Session.set('listdevice', JSON.parse(localStorage.getItem('scaledevices')))
                });
            }
        }, 15000);

    },
    "click #cleardevice"() {
        var scaledevices = [];
        scaledevices = JSON.parse(localStorage.getItem('scaledevices'));



        for (var i = scaledevices.length - 1; i >= 0; --i) {
            if (scaledevices[i].address == this.address) {
                scaledevices.splice(i, 1);
            }
        }
        console.log(scaledevices);
        localStorage.setItem('scaledevices', JSON.stringify(scaledevices))
        Session.set('listdevice', JSON.parse(localStorage.getItem('scaledevices')))
        M.toast({
            html: "ลบแล้ว "
        })
        // var total = 0;
        // for (let index = 0; index < waste.length; index++) {
        //     const element = waste[index];
        //     total += element.wasteweigh
        // }
        // Session.set('wastetotal', total);

        // Session.set('waste', waste);
    },
    'click #dis1'() {
        Session.set('dis1', "disabled")
        Session.set('dis2', "")
        Session.set('dis3', "")
        Session.set('dis4', "")
        Session.set('numdif', 0)
    },
    'click #dis2'() {
        Session.set('dis1', "")
        Session.set('dis2', "disabled")
        Session.set('dis3', "")
        Session.set('dis4', "")
        Session.set('numdif', 13.76)
    },
    'click #dis3'() {
        Session.set('dis1', "")
        Session.set('dis2', "")
        Session.set('dis3', "disabled")
        Session.set('dis4', "")
        Session.set('numdif', 5.00)
    },
    'click #dis4'() {
        var man_wei = localStorage.getItem('man_wei');
            console.log(man_wei == null);
            if (man_wei == null) {
                // console.log(man_wei, "555")
                $('#man_wei').val("0")
            } else {
                console.log(man_wei);

                $('#man_wei').val(man_wei)
                // Session.set('numdif', $('#man_wei').val())
            }
        Session.set('dis1', "")
        Session.set('dis2', "")
        Session.set('dis3', "")
        Session.set('dis4', "disabled")
        Session.set('numdif', $('#man_wei').val())
    },
    'keyup #man_wei'() {
        Session.set('numdif', $('#man_wei').val())
        localStorage.setItem('man_wei', $('#man_wei').val())
    },
    'change #man_wei'() {
        Session.set('numdif', $('#man_wei').val())
        localStorage.setItem('man_wei', $('#man_wei').val())
    }
});

Template.registerHelper('addonee', function (argument) {
    return argument + 1
});

Template.registerHelper('chzero', function (argument) {
    return parseFloat(argument).toFixed(2)
});