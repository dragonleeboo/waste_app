const axios = require('axios');
import Swal from 'sweetalert2'
Template.nfc.onRendered(function () {
    if (Meteor.isCordova) {
        MufuCaCa.enabled(function (data) {
            MufuCaCa.addTagDiscoveredListener(function (data) {
                var tag = MufuCaCa.bytesToHexString(data.tag.id);
                Session.set('readtag', tag)
                Session.set('showA', null);
                Session.set('showB', null);
                // var tag = MufuCaCa.bytesToHexString(data.tag.id)
                check_tag(tag);

            });

        }, function () {
            Swal.fire({
                title: "NFC ถูกปิดใช้งาน",
                text: "กรุณาเปิดใช้งานก่อนทุกครั้ง หรือติดต่อเจ้าหน้าที่",
                timer: 2000,
                showConfirmButton: false,
                icon: "error"
            }).then(() => {
                Router.go('/')
            });
        })
    }
})

Template.nfc.helpers({
    readtag: function () {
        return Session.get('readtag')
    },
    showA: function () {
        return Session.get('showA')
    },
    showB: function () {
        return Session.get('showB')
    },
});

Template.nfc.events({
    'click #clear': function (event, template) {
        Session.set('readtag', null)
        Session.set('showA', null);
        Session.set('showB', null);
    }
});

function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}


async function check_tag(tag) {
     myfuc.onload("nfc")
    let promiseA = axios.post(`${Meteor.settings.public.api_host}/check_person`, {
        rfid: tag
    })
    let resultA = await promiseA
    // console.log(resultA.data);



    if (resultA.data.length == 0) {
        let promiseB = axios.post(`${Meteor.settings.public.api_host}/check_car`, {
            rfid: tag
        })
        let resultB = await promiseB
        console.log(resultB.data);
        Session.set('showB', resultB.data[0]);
        myfuc.ondone("nfc")
    } else {
        if (resultA.data[0].job_type == "2") {
            console.log(resultA.data);
            Session.set('showA', resultA.data[0]);
        } else if (resultA.data[0].job_type == "3") {
            console.log(resultA.data);
            Session.set('showA', resultA.data[0]);
        } else if (resultA.data[0].job_type == "4") {
            console.log(resultA.data);
            Session.set('showA', resultA.data[0]);
        } else if (resultA.data[0].job_type == "1") {
            let promiseC = axios.post(`${Meteor.settings.public.api_host}/check_hospital`, {
                hospcode: resultA.data[0].company_code
            })
            let resultC = await promiseC
            var hm = resultA.data[0]
            hm.company_name = resultC.data[0].NAME
            console.log(hm);
            Session.set('showA', hm);
        }
        myfuc.ondone("nfc")
    }
}