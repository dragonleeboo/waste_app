import Swal from 'sweetalert2'
const axios = require('axios');
Template.factoryweigh.onRendered(function () {
    Session.set('bleSerial2', null);
    Session.set('saveone', null);
    Session.set('savetwo', null);
    Session.set('findwait', false);
    Session.set('find', null);
    Session.set('listdevice', JSON.parse(localStorage.getItem('scaledevices')))
    var list = Session.get('carcoming')
    var destroy = JSON.parse(localStorage.getItem('destroy'))
    console.log(destroy);
    if (destroy) {
        for (let index = 0; index < destroy.length; index++) {
            const element = destroy[index];
            if (list[0].car_id == element.car) {
                Session.set('saveone', element.weight_in);
            }
        }
    }
    if (Meteor.isCordova) {
        Session.set('stop', false);
        bluetoothSerial.disconnect()
        //     // alert(localStorage.getItem('bt_address'));
        //     if (localStorage.getItem('bt_address')) {
        //         bluetoothSerial.connect(localStorage.getItem('bt_address'), function (data) {
        //             M.toast({
        //                 html: "เชื่อมต่อสำเร็จ!!"
        //             })
        //             bluetoothSerial.subscribe('\n', function (data) {
        //                 var dataconvert = data.split('**=')[1]
        //                 var dataconvert2 = dataconvert.split(':**')[0]
        //                 Session.set('bleSerial', dataconvert2)
        //                 console.log(dataconvert2);
        //                 $("#wait").hide();
        //             });
        //             // check = 1;
        //         }, function (data) {
        //             M.toast({
        //                 html: data
        //             })

        //         });
        //     }
        // });
        // M.toast({
        //     html: "กำลังค้นหา ... "
        // })
        // bluetoothSerial.discoverUnpaired(function (data) {
        //     console.log(data);
        //     var check = 0;
        //     if (data) {
        //         for (let i = 0; i < data.length; i++) {
        //             const element = data[i];

        //             if (element.name) {
        //                 if (element.name == "MOCKUP_SCALE") {
        //                     // M.toast({html: "กำลังค้นหา"})
        //                     localStorage.setItem('bt_address', element.address)
        //                     M.toast({
        //                         html: "ค้นพบเครื่องชั่งแล้ว กำลังเชื่อมต่อ ... " + element.address
        //                     })
        //                     bluetoothSerial.connect(element.address, function (data) {
        //                         M.toast({
        //                             html: "เชื่อมต่อสำเร็จ!!"
        //                         })
        //                         bluetoothSerial.subscribe('\n', function (data) {
        //                             var dataconvert = data.split('**=')[1]
        //                             var dataconvert2 = dataconvert.split(':**')[0]
        //                             Session.set('bleSerial2', dataconvert2)
        //                             // console.log(dataconvert2);
        //                             $("#wait").hide();
        //                         });
        //                         check = 1;
        //                     }, function (data) {
        //                         if (data) {
        //                             M.toast({
        //                                 html: "กำลังค้นหา ..."
        //                             })
        //                         }
        //                         // M.toast({
        //                         //     html: data
        //                         // })
        //                     });
        //                 }
        //             }
        //         }
        //     }
        //     setTimeout(() => {
        //         console.log(check);
        //         if (check == 0) {
        //             Swal.fire({
        //                 title: "โปรดตรวจสอบ!",
        //                 text: "ไม่พบเครื่องชั่ง",
        //                 timer: 1000,
        //                 showConfirmButton: false,
        //                 icon: "warning"
        //             }).then(function () {
        //                 Router.go('/')
        //             });
        //         }
        //     }, 30000);
        //     });
    }
});
Template.factoryweigh.helpers({
    bleSerial: function () {
        return Session.get('bleSerial2')
    },
    saveone: function () {
        return Session.get('saveone')
    },
    savetwo: function () {
        return Session.get('savetwo')
    },
    total() {
        if (Session.get('saveone'), Session.get('savetwo')) {
            var total = Math.abs(parseFloat(Session.get('savetwo')) - parseFloat(Session.get('saveone')))
            // var total = parseFloat(Session.get('saveone')) - car;
            // alert(parseFloat(Session.get('savetwo')) +"," +parseFloat(Session.get('saveone')))
            return total.toFixed(2)
        }
    },
    carcoming: function () {
        return Session.get('carcoming')[0]
    },
    sum_weight() {
        var list = Session.get('carcoming')
        var weight = 0;

        for (let index = 0; index < list.length; index++) {
            const element = list[index].walkout_weight;
            weight += element
        }
        return weight

    },
    persen() {
        var weight_fac = Math.abs(parseFloat(Session.get('savetwo')) - parseFloat(Session.get('saveone')))
        var list = Session.get('carcoming')
        var weight_transac = 0
        for (let index = 0; index < list.length; index++) {
            const element = list[index].walkout_weight;
            weight_transac += element
        }
        var persen = ((weight_fac / weight_transac) * 100) - 100

        // console.log(weight,max,persen);
        Session.set('persen', persen)
        return persen
    },
    find() {
        return Session.get('find')
    },
    findwait() {
        return Session.get('findwait')
    },
    listdevice() {
        return Session.get('listdevice')
    }
});

Template.factoryweigh.events({
    'click #saveone': function (event, template) {
        Session.set('saveone', Session.get('bleSerial2'));
        var destroy = []
        if (JSON.parse(localStorage.getItem('destroy'))) {
            destroy = JSON.parse(localStorage.getItem('destroy'))
        }
        destroy.push({
            "car": Session.get('carcoming')[0].car_id,
            "weight_in": Session.get('bleSerial2'),
            "type": 2
        })
        localStorage.setItem('destroy', JSON.stringify(destroy))
    },
    'click #savetwo': function (event, template) {

        Session.set('savetwo', Session.get('bleSerial2'));
    },
    'click #confirm'() {
        myfuc.onload("factoryweigh")
        var list = Session.get('carcoming');
        // console.log(list);
        var destroy_time = moment().tz("Asia/Bangkok").format('YYYY-MM-DD HH:mm:ss');
        var report_id_obj = []
        for (let index = 0; index < list.length; index++) {
            const element = list[index]
            // weight += element.walkout_weight;
            report_id_obj.push({
                "report_id": element.report_id,
                "walkout_weight": element.walkout_weight
            })
        }
        console.log({
            list: report_id_obj,
            persen: Session.get('persen'),
            factory: Session.get('factory'),
            destroy_time: destroy_time
        });

        axios.post(`${Meteor.settings.public.api_host}/torsor_destroy`, {
                list: report_id_obj,
                persen: Session.get('persen'),
                factory: Session.get('factory'),
                destroy_time: destroy_time
            })

            .then(function (response) {
                Swal.fire({
                    title: "สำเร็จ!",
                    text: "บันทึกรายการเรียบร้อย",
                    timer: 2000,
                    showConfirmButton: false,
                    icon: "success"
                }).then(function () {
                    var destroy = []
                    console.log('destroy', destroy);
                    if (JSON.parse(localStorage.getItem('destroy'))) {
                        destroy = JSON.parse(localStorage.getItem('destroy'))
                    }
                    for (var i = destroy.length - 1; i >= 0; --i) {
                        if (destroy[i].car == list[0].car_id) {
                            destroy.splice(i, 1);
                        }
                    }
                    console.log('destroy', destroy);
                    localStorage.setItem('destroy', JSON.stringify(destroy))

                    bluetoothSerial.write("C");
                    Session.set('stop', true);
                });
            })
            .catch(function (error) {
                //console.log(error);
            });
        // for (let index = 0; index < array.length; index++) {
        //     const element = array[index];

        // }

    },
    'click #find'() {
        M.toast({
            html: "กำลังค้นหา ... "
        })
        Session.set('find', null);
        Session.set('findwait', true);
        bluetoothSerial.discoverUnpaired(function (data) {
            console.log(data);
            var devices = []
            for (let index = 0; index < data.length; index++) {
                const element = data[index];
                if (element.class == "272") {
                    devices.push(element)
                }
            }
            Session.set('find', devices);
            Session.set('findwait', false);
        });
    },
    "click #selectfind"() {
        var scaledevices = [];
        if (localStorage.getItem('scaledevices')) {
            scaledevices = JSON.parse(localStorage.getItem('scaledevices'));
        }
        var findevices = this

        var check = true;
        if (scaledevices.length > 0) {
            console.log('findevices', findevices);
            console.log('scaledevices', scaledevices);
            for (let old = 0; old < scaledevices.length; old++) {
                const olddevices = scaledevices[old];
                // for (let news = 0; news < findevices.length; news++) {
                //     const newdevices = findevices[news];
                // console.log(findevices.address, olddevices.address);
                if (this.address == olddevices.address) {
                    check = false
                }
                // }

            }
        }
        // console.log(check);

        if (check) {
            scaledevices.push(this)
            console.log(scaledevices);
            localStorage.setItem('scaledevices', JSON.stringify(scaledevices))
            Session.set('listdevice', JSON.parse(localStorage.getItem('scaledevices')))
            M.toast({
                html: "เชื่อมต่อสำเร็จ "
            })
        }
        Session.set('find', null)

        // localStorage.setItem('scaledevices', JSON.stringify(resultB.data[0]))
        // Session.set('car', JSON.parse(localStorage.getItem('car')))
    },
    "click #selectdevice"() {
        var check = 0;
        myfuc.onload("factoryweigh")
        bluetoothSerial.connect(this.address, function (data) {
            M.toast({
                html: "เชื่อมต่อแล้ว"
            })
            // console.log(data);

            bluetoothSerial.subscribe('\n', function (data) {
                console.log(data);
                if (data[0] == "A") {
                    if (Session.get('stop')) {
                        bluetoothSerial.disconnect();
                        Router.go('factory')
                        Session.set('stop', false);
                        myfuc.ondone("factoryweigh")
                    }
                }
                var dataconvert = data.split('**=')[1]
                var dataconvert2 = dataconvert.split(':**')[0]
                if (dataconvert2) {
                    myfuc.ondone("factoryweigh")
                    Session.set('bleSerial2', dataconvert2)
                    // console.log(dataconvert2);
                    $(".listdevices").hide();
                } else {
                    $(".listdevices").hide();
                    bluetoothSerial.disconnect();
                    Session.set('bleSerial2', null);
                    Session.set('waste', []);
                    Session.set('wastetotal', null);
                    Session.set('findwait', false);
                    Session.set('find', null);
                    Session.set('listdevice', JSON.parse(localStorage.getItem('scaledevices')))
                }

            });
            check = 1;
        }, function (data) {
            if (data) {
                // M.toast({
                //     html: "กำลังค้นหา ..."
                // })
            }
        });

        setTimeout(() => {
            console.log(check);
            if (check == 0) {
                Swal.fire({
                    title: "โปรดตรวจสอบ!",
                    text: "ไม่พบเครื่องชั่ง",
                    timer: 1000,
                    showConfirmButton: false,
                    icon: "warning"
                }).then(function () {
                    // Router.go('/')
                    myfuc.ondone("weigh")
                    $(".listdevices").show();
                    bluetoothSerial.disconnect();
                    Session.set('bleSerial', null);
                    Session.set('waste', []);
                    Session.set('wastetotal', null);
                    Session.set('findwait', false);
                    Session.set('find', null);
                    Session.set('listdevice', JSON.parse(localStorage.getItem('scaledevices')))
                });
            }
        }, 15000);

    },
    "click #back"() {
        // console.log('d');
        bluetoothSerial.write("C");
        Session.set('stop', true);
        myfuc.onload("factoryweigh")
    }
});