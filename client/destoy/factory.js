import Swal from 'sweetalert2'
const axios = require('axios');
Template.factory.onRendered(function () {
    // Session.set('factory',null)
    Session.set('carcoming',null)

    Session.set('factory', JSON.parse(localStorage.getItem('factory')))
    if (Meteor.isCordova) {
        bluetoothSerial.disconnect()        
        MufuCaCa.enabled(function (data) {
            MufuCaCa.addTagDiscoveredListener(function (data) {
                var tag = MufuCaCa.bytesToHexString(data.tag.id)
                console.log(tag);
                check_tag(tag);
            });

        }, function () {
            alert("NFC ถูกปิดใช้งาน กรุณาเปิดใช้งานก่อนทุกครั้ง")
        })
    }
});
Template.factory.helpers({ 
    factory: function() { 
        return Session.get('factory')
    }, 
    carcoming: function() { 
        return Session.get('carcoming')[0]
    }, 
    checkstate: function() { 
         if(Session.get('factory')&&Session.get('carcoming')){
            return true
         }
    },
    sum_weight() {
        var list = Session.get('carcoming')
        var weight = 0;
       
        for (let index = 0; index < list.length; index++) {
            const element = list[index].walkout_weight;
            weight += element
        }
        return weight

    }
}); 

Template.factory.events({ 
    'click #clearfactory': function(event, template) { 
        localStorage.removeItem('factory')
        Session.set('factory',null)
    } ,
    'click #clearcarcoming'(){
        Session.set('carcoming',null)
    },
    'click #confirm'(){
        Router.go('factoryweigh')
    },
    'click #confirm2'(){
        Router.go('factoryweighkeyin')
    }


    
}); 


async function check_tag(tag) {
    myfuc.onload('factory')
    
    let promiseA = axios.post(`${Meteor.settings.public.api_host}/check_person`, {
        rfid: tag
    })
    let resultA = await promiseA
    console.log(resultA.data);
    if (resultA.data.length == 0) {
        let promiseB = axios.post(`${Meteor.settings.public.api_host}/findcarfortransfer`, {
            rfid: tag
        })
        let resultB = await promiseB
        console.log(resultB);
        myfuc.ondone('factory')
        if (resultB.data.length == 0) {
            Swal.fire({
                title: "ไม่พบข้อมูลบัตร!",
                text: "โปรดลองอีกครั้ง หรือติดต่อเจ้าหน้าที่",
                timer: 1000,
                showConfirmButton: false,
                icon: "error"
            });
        }else{
            Swal.fire({
                title: "สำเร็จ!",
                text: resultB.data[0].car_id,
                timer: 1000,
                showConfirmButton: false,
                icon: "success"
            }).then(function(){
                if (Session.get('factory') && Session.get('carcoming')) {
                    var list = Session.get('carcoming')
                    var destroy = JSON.parse(localStorage.getItem('destroy'))
                    console.log("destroy", destroy);
                    if (destroy) {
                        for (let index = 0; index < destroy.length; index++) {
                            const element = destroy[index];
                            if (list[0].car_id == element.car) {
                                if (element.type == 1) {
                                    Router.go('/factoryweighkeyin')
                                }else if(element.type == 2){
                                    Router.go('/factoryweigh')
                                }
                            }
                        }
                    }
                }
            });
            Session.set('carcoming', resultB.data)
            
        }
    } else {
        if (resultA.data[0].job_type == "4") {
            Swal.fire({
                title: "สำเร็จ!",
                text: resultA.data[0].prename + resultA.data[0].firstname + " " + resultA.data[0].lastname,
                timer: 1000,
                showConfirmButton: false,
                icon: "success"
            }).then(function(){
                if (Session.get('factory') && Session.get('carcoming')) {
                    var list = Session.get('carcoming')
                    var destroy = JSON.parse(localStorage.getItem('destroy'))
                    console.log("destroy", destroy);
                    if (destroy) {
                        for (let index = 0; index < destroy.length; index++) {
                            const element = destroy[index];
                            if (list[0].car_id == element.car) {
                                if (element.type == 1) {
                                    Router.go('/factoryweighkeyin')
                                }else if(element.type == 2){
                                    Router.go('/factoryweigh')
                                }
                            }
                        }
                    }
                }
            });
            myfuc.ondone('factory')
            localStorage.setItem('factory', JSON.stringify(resultA.data[0]))
            Session.set('factory', JSON.parse(localStorage.getItem('factory')))
        } else{
            myfuc.ondone('factory');
            Swal.fire({
                title: "ไม่พบข้อมูลบัตร!",
                text: "โปรดลองอีกครั้ง หรือติดต่อเจ้าหน้าที่",
                timer: 1000,
                showConfirmButton: false,
                icon: "error"
            });
        }
    }
}