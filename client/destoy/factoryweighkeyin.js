import Swal from 'sweetalert2'
const axios = require('axios');
Template.factoryweighkeyin.onRendered(() => {
    var list = Session.get('carcoming')
    var weight = 0;

    for (let index = 0; index < list.length; index++) {
        const element = list[index].walkout_weight;
        weight += element
    }
    Session.set('sum_weight', weight)
    Session.set('persen', null)
    Session.set('weight_fac', null);
    Session.set('confirm1', null);
    Session.set('saveone', null);
    Session.set('confirm2', null);
    Session.set('savetwo', null);
    Session.set('makeid', makeid(10))
    $(document).ready(function () {
        $('.materialboxed').materialbox();
    });

    var destroy = JSON.parse(localStorage.getItem('destroy'))
    console.log(destroy);
    if (destroy) {
        for (let index = 0; index < destroy.length; index++) {
            const element = destroy[index];
            if (list[0].car_id == element.car) {
                Session.set('confirm1', true);
                Session.set('saveone', element.weight_in);
                $("#pic1").attr("src", element.pic);
            }
        }


    }

})

Template.factoryweighkeyin.helpers({
    saveone() {
        return Session.get('saveone')
    },
    savetwo() {
        return Session.get('savetwo')
    },
    carcoming: function () {
        return Session.get('carcoming')[0]
    },
    sum_weight() {
        return Session.get('sum_weight')
    },
    weight_fac() {
        console.log(Session.get('saveone'), Session.get('savetwo'));
        if (Session.get('saveone'), Session.get('savetwo')) {
            var total = Math.abs(parseFloat(Session.get('savetwo')) - parseFloat(Session.get('saveone')))
            return total.toFixed(2)
        }
    },
    persen() {
        return Session.get('persen')
    },
    confirm1() {
        return Session.get('confirm1')
    },
    confirm2() {
        return Session.get('confirm2')
    },
    persen() {
        var weight_fac = Math.abs(parseFloat(Session.get('savetwo')) - parseFloat(Session.get('saveone')))
        var list = Session.get('carcoming')
        var weight_transac = 0
        for (let index = 0; index < list.length; index++) {
            const element = list[index].walkout_weight;
            weight_transac += element
        }
        var persen = ((weight_fac / weight_transac) * 100) - 100

        // console.log(weight,max,persen);
        Session.set('persen', persen)
        return persen
    },
});

Template.factoryweighkeyin.events({
    'click #confirm1': function (event, template) {
        if ($('#weight_fac1').val()) {
            myfuc.onload('factoryweighkeyin')
            if (Meteor.isCordova) {
                navigator.camera.getPicture(onSuccess, onFail, {
                    quality: 50,
                    destinationType: Camera.DestinationType.DATA_URL,
                    targetWidth: 600,
                    // allowEdit: true,
                    correctOrientation: true
                });

                function onSuccess(imageData) {
                    var ima_base64 = "data:image/jpeg;base64," + imageData;
                    $("#pic1").attr("src", ima_base64);
                    var name_id = Session.get('makeid')
                    axios.post(`${Meteor.settings.public.api_host}/destroypic_set`, {
                            name: name_id,
                            PIC_VERIFY: ima_base64,
                            type: 1
                        })
                        .then(function (response) {
                            myfuc.ondone('factoryweighkeyin')
                            // Session.set('confirm1', true);
                            // Session.set('saveone', $('#weight_fac1').val());
                            var destroy = []
                            if (JSON.parse(localStorage.getItem('destroy'))) {
                                destroy = JSON.parse(localStorage.getItem('destroy'))
                            }
                            destroy.push({
                                "car": Session.get('carcoming')[0].car_id,
                                "weight_in": $('#weight_fac1').val(),
                                "type": 1,
                                "pic": ima_base64
                            })
                            localStorage.setItem('destroy', JSON.stringify(destroy))
                            Session.set('confirm1', true);
                            Session.set('saveone', $('#weight_fac1').val());
                            // Session.set('destroy', JSON.parse(localStorage.getItem('factory')))
                        })
                        .catch(function (error) {
                            //console.log(error);
                        });
                }

                function onFail(message) {
                    alert('Failed because: ' + message);
                }
            }
        }

    },
    'click #confirm2': function (event, template) {
        if ($('#weight_fac2').val()) {
            myfuc.onload('factoryweighkeyin')
            if (Meteor.isCordova) {
                navigator.camera.getPicture(onSuccess, onFail, {
                    quality: 50,
                    destinationType: Camera.DestinationType.DATA_URL,
                    targetWidth: 600,
                    // allowEdit: true,
                    correctOrientation: true
                });

                function onSuccess(imageData) {
                    var ima_base64 = "data:image/jpeg;base64," + imageData;
                    // console.log(ima_base64);
                    $("#pic2").attr("src", ima_base64);
                    var name_id = Session.get('makeid')
                    axios.post(`${Meteor.settings.public.api_host}/destroypic_set`, {
                            name: name_id,
                            PIC_VERIFY: ima_base64,
                            type: 2
                        })
                        .then(function (response) {
                            myfuc.ondone('factoryweighkeyin')
                            Session.set('confirm2', true);
                            Session.set('savetwo', $('#weight_fac2').val())
                        })
                        .catch(function (error) {
                            //console.log(error);
                        });
                }

                function onFail(message) {
                    alert('Failed because: ' + message);
                }
            }
        }

    },



    'click #confirm3': function (event, template) {
        myfuc.onload('factoryweighkeyin')
        var weight_fac = Math.abs(parseFloat(Session.get('savetwo')) - parseFloat(Session.get('saveone')))
        var list = Session.get('carcoming')
        var weight_transac = 0
        for (let index = 0; index < list.length; index++) {
            const element = list[index].walkout_weight;
            weight_transac += element
        }
        var persen = ((weight_fac / weight_transac) * 100) - 100
        var name_id = Session.get('makeid')
        var list = Session.get('carcoming');
        var destroy_time = moment().tz("Asia/Bangkok").format('YYYY-MM-DD HH:mm:ss');
        var factory = JSON.parse(localStorage.getItem('factory'))
        Session.set('weight_fac', weight_fac);
        Session.set('persen', persen);
        var report_id_obj = []
        for (let index = 0; index < list.length; index++) {
            const element = list[index]
            // weight += element.walkout_weight;
            report_id_obj.push({
                "report_id": element.report_id,
                "walkout_weight": element.walkout_weight
            })
        }
        console.log({
            list: report_id_obj,
            persen: persen,
            factory: factory,
            destroy_time: destroy_time,
            status3: name_id
        });
        var destroy = []
        console.log('destroy', destroy);
        if (JSON.parse(localStorage.getItem('destroy'))) {
            destroy = JSON.parse(localStorage.getItem('destroy'))
        }
        for (var i = destroy.length - 1; i >= 0; --i) {
            if (destroy[i].car == list[0].car_id) {
                destroy.splice(i, 1);
            }
        }
        console.log('destroy', destroy);
        localStorage.setItem('destroy', JSON.stringify(destroy))
        axios.post(`${Meteor.settings.public.api_host}/torsor_destroy`, {
                list: report_id_obj,
                persen: Session.get('persen'),
                factory: factory,
                destroy_time: destroy_time,
                status3: name_id
            })
            .then(function (response) {
                console.log({
                    list: report_id_obj,
                    persen: Session.get('persen'),
                    factory: factory,
                    destroy_time: destroy_time,
                    status3: name_id
                });
                Swal.fire({
                    title: "สำเร็จ!",
                    text: "บันทึกรายการเรียบร้อย",
                    timer: 2000,
                    showConfirmButton: false,
                    icon: "success"
                }).then(function () {


                    myfuc.ondone('factoryweighkeyin')
                    Router.go('/')
                });
            })
            .catch(function (error) {
                //console.log(error);
            });
    },

    'click #cancel': function (event, template) {

        Swal.fire({
                title: "ต้องการยกเลิก ?",
                text: "ข้อมูลการทำรายกาารจะหายทั้งหมด",
                type: "warning",
                icon: "warning",
                confirmButtonText: "ใช่แล้ว",
                cancelButtonText: "ไม่ใช่",
                showCancelButton: true
            })
            .then((result) => {
                console.log(result.value);
                if (result.value) {
                    Swal.fire({
                        title: "สำเร็จ!",
                        text: "ยกเลิกรายการเรียบร้อย",
                        timer: 2000,
                        showConfirmButton: false,
                        icon: "success"
                    }).then(function () {
                        Router.go('/')
                    });
                } else if (result.dismiss === 'cancel') {

                }
            })
    },

});


function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}