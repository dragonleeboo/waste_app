Template.registerHelper("DateTime", function (data) {
    if (data && data != '0000-00-00') {
        var dm = moment(data).format("DD/MM/");
        var y = parseInt(moment(data).format("YYYY")) + 543 - 2500;
        var h = moment(data).format(" HH:mm");
        var date = dm + y +h+" น.";
        return date;
    } else {
        return "-";
    }
});

Template.registerHelper("getCommaSeparatedTwoDecimalsNumber", function (data) {
    if (data ) {
        const fixedNumber = Number.parseFloat(data).toFixed(2);
    return String(fixedNumber).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    } else {
        return "-";
    }
});

Template.registerHelper("addone", function (data) {
    if (data) {
        var i = parseInt(addone) + 1
        return i+"."
    }
});


