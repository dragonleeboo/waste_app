import {
    Template
} from 'meteor/templating';

import './main.html';
import '../node_modules/materialize-css/dist/css/materialize'
import '../node_modules/materialize-css/dist/js/materialize'
import Swal from 'sweetalert2'
import '../node_modules/@fortawesome/fontawesome-free/js/all.js'
import '../node_modules/animate.css/animate.min.css'

Template.body.onRendered(function () {
    if (Meteor.isCordova) {
        document.addEventListener('deviceready', function () {
            StatusBar.backgroundColorByHexString('#1e88e5');
            // StatusBar.styleDefault(); // font black
            StatusBar.styleLightContent(); // font white
        });
        if (window.MobileAccessibility) {
            window.MobileAccessibility.usePreferredTextZoom(false);
        }
        $("html").click(function () {
            navigator.vibrate(50);
        });

        document.addEventListener("backbutton", function () {
            event.preventDefault();
        }, false);
    }
})